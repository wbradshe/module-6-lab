#!/bin/bash
#ALGO:
#Display name of movie
#
#print each line like this
#
#NAME: LINE
#
#Replace vader with Big Daddy. (Darth Big Daddy).
##########################################################
#Pseudo Code:
#
#
#curl URL | grep stuff | cut away things
#
#print in file.
#
# 
###############################################################

#Playing around with the data.


#curl https://github.com/gastonstat/StarWars/blob/master/Text_files/SW_EpisodeIV.txt | grep -P 'LUKE&quot; &quot;[.,?!#\w\d ]*&quot;</td>' | cut -d ";" -f6 | cut -d "&" -f1 >> SW_EpisodeIV_Script.txt

#curl https://github.com/gastonstat/StarWars/blob/master/Text_files/SW_EpisodeV.txt | grep -P 'LUKE&quot; &quot;[.,?!#\w\d ]*&quot;</td>' | cut -d ";" -f6 | cut -d "&" -f1 >> LUKEQUOTES.txt

#NumberofLines= wc -l LUKEQUOTES.txt | cut -d " " -f1
#echo $NumberofLines
#curl https://github.com/gastonstat/StarWars/blob/master/Text_files/SW_EpisodeVI.txt | grep -P '[\w\d]&quot; &quot;[.,?!#\w\d ]*&quot;</td>' | cut -d ";" -f46 | cut -d "&" -f1


#cat LUKEQUOTES.txt | sed "s/[Vv]ader/Little Anny/" | sed "s/[Ff]ather/DADDY ;)/"

#grabs names
#curl https://github.com/gastonstat/StarWars/blob/master/Text_files/SW_EpisodeVI.txt | grep -P '[\w\d]&quot; &quot;[.,?!#\w\d ]*&quot;</td>' | cut -d ";" -f4 | cut -d "&" -f1
####################################################################

#prints the script but replaces all cases of the words father, vader, FATHER, VADER, anakin, ANAKIN. (This is episode VI so vaders last few lines are as anakin).
curl https://github.com/gastonstat/StarWars/blob/master/Text_files/SW_EpisodeVI.txt | grep -P '[\w\d]&quot; &quot;[.,?!#\w\d ]*&quot;</td>' | cut -d ";" -f4-6 | sed 's/&quot; &quot;/: /' | sed 's/&quot//' | sed 's/[fFvVAa][AaNn][DdTtAa][HhEeKk][EeRrIi]*[RrNn]*/Big Daddy/' >> episode_VI_BigDaddy_Script.txt

curl https://github.com/gastonstat/StarWars/blob/master/Text_files/SW_EpisodeV.txt | grep -P '[\w\d]&quot; &quot;[.,?!#\w\d ]*&quot;</td>' | cut -d ";" -f4-6 | sed 's/&quot; &quot;/: /' | sed 's/&quot//' | sed 's/[fFvVAa][AaNn][DdTtAa][HhEeKk][EeRrIi]*[RrNn]*/Big Daddy/' >> episode_V_BigDaddy_Script.txt

curl https://github.com/gastonstat/StarWars/blob/master/Text_files/SW_EpisodeIV.txt | grep -P '[\w\d]&quot; &quot;[.,?!#\w\d ]*&quot;</td>' | cut -d ";" -f4-6 | sed 's/&quot; &quot;/: /' | sed 's/&quot//' | sed 's/[fFvVAa][AaNn][DdTtAa][HhEeKk][EeRrIi]*[RrNn]*/Big Daddy/' >> episode_IV_BigDaddy_Script.txt
